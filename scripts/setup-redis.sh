mkdir -p /pv/hostredis
chmod 777 /pv/hostredis
podman run -d -p 6379:6379 --name redis \
    -v /pv/hostredis:/var/lib/redis/data:Z \
    registry.access.redhat.com/rhscl/redis-32-rhel7
