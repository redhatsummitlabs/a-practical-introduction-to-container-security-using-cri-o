#!/bin/bash
#
sudo podman run --name=labmanual --detach -p 8080:8080 -e WORKSHOPS_URLS="https://gitlab.com/redhatsummitlabs/a-practical-introduction-to-container-security-using-cri-o/raw/master/_workshop1.yml" quay.io/osevg/workshopper:latest
